﻿using UnityEngine;
using System.Collections;
using Utility_Collection;

public class Pickup_Controller : MonoBehaviour
{

    public Pickup_Type pickup_Type;
    public float life_Time;

    private Transform m_Transform;
    private Rigidbody2D m_Rigidbody;
    private Collider2D m_Collider;
    private Pickup_Manager m_Pickup_Manager;
    private Player_Manager m_Player_Manager;
    private float m_Value;
    private float m_Life_Timer;


    public void Intialize(Pickup_Manager pickup_Manager, Player_Manager player_Manager, float value)
    {
        m_Transform = GetComponent<Transform>();
        m_Rigidbody = m_Transform.GetComponent<Rigidbody2D>();
        m_Rigidbody.isKinematic = true;
        m_Collider = m_Transform.GetComponent<Collider2D>();
        m_Collider.enabled = false;
        m_Pickup_Manager = pickup_Manager;
        m_Player_Manager = player_Manager;
        m_Value = value;
        m_Life_Timer = life_Time;
    }


    public void Activate_Object()
    {
        m_Rigidbody.isKinematic = false;
        m_Collider.enabled = true;
        m_Life_Timer = life_Time;
    }


    private void Update ()
    {
        if (m_Life_Timer > 0.0f)
        {
            m_Life_Timer -= Time.deltaTime;
            if (m_Life_Timer <= 0.0f)
            {
                m_Rigidbody.velocity = Vector2.zero;
                m_Rigidbody.isKinematic = true;
                m_Collider.enabled = false;
                m_Pickup_Manager.Remove_Object(m_Transform, pickup_Type);
            }
        }
    }


    private void OnCollisionEnter2D (Collision2D collision)
    {
        if (collision.collider.tag == "Player")
        {
            m_Rigidbody.velocity = Vector2.zero;
            m_Rigidbody.isKinematic = true;
            m_Collider.enabled = false;
            m_Player_Manager.On_Object_Pickup(collision.collider.transform, m_Value);
            m_Pickup_Manager.Remove_Object(m_Transform, pickup_Type);
        }
    }

}



