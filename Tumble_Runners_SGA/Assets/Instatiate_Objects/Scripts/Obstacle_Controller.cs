﻿using UnityEngine;
using System.Collections;
using Utility_Collection;


public class Obstacle_Controller : MonoBehaviour
{

    public Obstacle_Type obstacle_Type;
    public float shrink_Scale;
    public float shrink_Speed;

    private Transform m_Transform;
    private Rigidbody2D m_Rigidbody;
    private Collider2D m_Collider;
    private Obstacle_Manager m_Obstacle_Manager;
    private float m_Life_Timer;
    private Vector2 m_scale;
   

    public void Intialize(Obstacle_Manager obstacle_Manager)
    {
        m_Transform = GetComponent<Transform>();
        m_Rigidbody = m_Transform.GetComponent<Rigidbody2D>();
        m_Rigidbody.isKinematic = true;
        m_Collider = m_Transform.GetComponent<Collider2D>();
        m_Collider.enabled = false;
        m_Obstacle_Manager = obstacle_Manager;
        m_scale = m_Transform.localScale;
    }


    public void Activate_Object(float life_Time)
    {
        m_Rigidbody.isKinematic = false;
        m_Collider.enabled = true;
        m_Life_Timer = life_Time;
    }


    private void Update ()
    {
        if (m_Life_Timer > 0.0f)
        {
            m_Life_Timer -= Time.deltaTime;

            if (m_Life_Timer <= 0.0f)
            {
                m_Rigidbody.isKinematic = true;
                m_Collider.enabled = false;
                StartCoroutine(Object_Disapear());
            }
        }
    }


    IEnumerator Object_Disapear()
    {
        Vector2 target_Scale = Vector2.one * shrink_Scale;
        
        while ((Vector2)m_Transform.localScale != target_Scale)
        {
            m_Transform.localScale = (Vector3)Math_Ex.Const_Vctr_Lerp(
                (Vector2)m_Transform.lossyScale, 
                target_Scale,
                shrink_Speed,
                Time.deltaTime
            );

            if (Vector2.Distance(m_Transform.lossyScale, target_Scale) <= 0.05f)
            {
                Transform instance = Instantiate(Resources.Load("Obstacles/Obstacle_Disappear", typeof(Transform))) as Transform;
                instance.position = m_Transform.position;
                break;
            }
            yield return null;
        }
        m_Obstacle_Manager.Remove_Object(m_Transform, obstacle_Type, m_scale);
    }


}



