﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Utility_Collection;
using Random = UnityEngine.Random;


public class Pickup_Manager : MonoBehaviour
{

    public int instances_Per_Player;
    public float wave_Spawn_Time;
    public float spawn_In_Time;
    public float spawn_Impulse_Force;
    public float coin_Value;
    public float ring_Value;

    private Transform m_Transform;
    private Transform m_Instance_Hold_Transform;
    private Transform m_Spawn_Transform;
    private Player_Manager m_Player_Manager;
    private List<Transform> m_Coin_Instance;
    private List<Transform> m_Ring_Instance;
    private Rotation_Direction m_Rotation_Direction;
    public float m_Wave_Spawn_Timer;
    private bool m_Game_Start;

    private void Awake ()
    {
        m_Transform = GetComponent<Transform>();
        m_Instance_Hold_Transform = GameObject.FindGameObjectWithTag("Pickup_Holder").transform;
        m_Spawn_Transform = GameObject.FindGameObjectWithTag("Pickup_Spawn_Point").transform;
        m_Player_Manager = m_Transform.GetComponent<Player_Manager>();

        m_Coin_Instance = new List<Transform>();
        m_Ring_Instance = new List<Transform>();

        
    }


    private void Start ()
    {
        m_Rotation_Direction = Rotation_Direction.Left;
        m_Wave_Spawn_Timer = wave_Spawn_Time;
    }

    
    private void Update ()
    {
        if (!m_Game_Start)
            return;

        if (m_Wave_Spawn_Timer > 0.0f)
        {
            m_Wave_Spawn_Timer -= Time.deltaTime;
            if (m_Wave_Spawn_Timer <= 0.0f)
            {
                StartCoroutine(Spawn_Pickup());
                m_Wave_Spawn_Timer = wave_Spawn_Time * 2.0f;
            }
        }
    }


    public void Remove_Object(Transform object_Transform, Pickup_Type pickup_Type)
    {
        object_Transform.SetParent(m_Instance_Hold_Transform);
        object_Transform.localPosition = Vector2.zero;

        if (pickup_Type == Pickup_Type.Coin)
            m_Coin_Instance.Add(object_Transform);
        else if (pickup_Type == Pickup_Type.Ring)
            m_Ring_Instance.Add(object_Transform);
    }


    public void On_Direction_Change (Rotation_Direction rotation_Direction)
    {
        Vector3 curr_Pos = m_Spawn_Transform.position;
        curr_Pos.x = -curr_Pos.x;
        m_Spawn_Transform.position = curr_Pos;
    }


    private IEnumerator Spawn_Pickup ()
    {
        int amount = instances_Per_Player * m_Player_Manager.Active_Player_Amount;
        int spawned_In = 0;

        while (spawned_In <= amount)
        {
            int rand = Random.Range(1, 10);
            if (rand % 3 == 0)
            {
                Spawn_In_Type(Pickup_Type.Ring);
            }
            else
            {
                Spawn_In_Type(Pickup_Type.Coin);
            }
            yield return new WaitForSeconds(spawn_In_Time);
            spawned_In++;
        }
    }


    private void Spawn_In_Type(Pickup_Type pickup_Type)
    {
        Vector2 force_Direction = get_Random_Direction(m_Rotation_Direction);

        if (pickup_Type == Pickup_Type.Coin)
        {
            Transform _object = m_Coin_Instance[0];
            m_Coin_Instance.Remove(_object);
            _object.position = m_Spawn_Transform.position;
            _object.SetParent(null);
            _object.GetComponent<Pickup_Controller>().Activate_Object();
            _object.GetComponent<Rigidbody2D>().AddForce(force_Direction * spawn_Impulse_Force, ForceMode2D.Impulse);
        }
        else if (pickup_Type == Pickup_Type.Ring)
        {
            Transform _object = m_Ring_Instance[0];
            m_Ring_Instance.Remove(_object);
            _object.position = m_Spawn_Transform.position;
            _object.SetParent(null);
            _object.GetComponent<Pickup_Controller>().Activate_Object();
            _object.GetComponent<Rigidbody2D>().AddForce(force_Direction * spawn_Impulse_Force, ForceMode2D.Impulse);
        }
    }


    private Vector2 get_Random_Direction(Rotation_Direction rotation_Direction)
    {
        Vector2 rand_Point = Random.insideUnitCircle;

        if (rotation_Direction == Rotation_Direction.Left && rand_Point.x > 0.0f)
        {
            rand_Point.x = -rand_Point.x;
        }
        else if (rotation_Direction == Rotation_Direction.Right  && rand_Point.x < 0.0f)
        {
            rand_Point.x = Mathf.Abs(rand_Point.x);
        }

        return rand_Point.normalized;
    }


    public void Initialize (int player_Amount)
    {
        int amount = instances_Per_Player * player_Amount;

        for (int i = 0; i < amount; i++)
        {
            Transform coin_Transform = Instantiate(Resources.Load("Pickups/Pickup_Coin", typeof(Transform))) as Transform;
            coin_Transform.GetComponent<Pickup_Controller>().Intialize(this, m_Player_Manager, coin_Value);
            coin_Transform.SetParent(m_Instance_Hold_Transform);
            coin_Transform.localPosition = Vector3.zero;
            m_Coin_Instance.Add(coin_Transform);

            Transform ring_Transform = Instantiate(Resources.Load("Pickups/Pickup_Ring", typeof(Transform))) as Transform;
            ring_Transform.GetComponent<Pickup_Controller>().Intialize(this, m_Player_Manager, coin_Value);
            ring_Transform.SetParent(m_Instance_Hold_Transform);
            ring_Transform.localPosition = Vector3.zero;
            m_Ring_Instance.Add(ring_Transform);
        }
    }


    public void On_Game_Start()
    {
        m_Game_Start = true;
    }


    public void On_Game_End()
    {
        m_Game_Start = false;
    }


}



