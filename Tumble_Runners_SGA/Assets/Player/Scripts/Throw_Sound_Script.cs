﻿using UnityEngine;
using System.Collections;

public class Throw_Sound_Script : MonoBehaviour
{
    public AudioClip m_Throw_Sound;
    public AudioSource source;

    // Use this for initialization
    void Start ()
    {
        source = GameObject.Find("ThrowAudio").GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        source.PlayOneShot(m_Throw_Sound);
    }
}
