﻿using UnityEngine;
using System.Collections;
using Player_Collection;
using Utility_Collection;


public class Player_Controller : MonoBehaviour
{

    public static float get_Horizontal_Axis(Player_ID player_ID)
    {
        var value = 0.0f;

        switch (player_ID)
        {
            case Player_ID.One:
                value = Input.GetAxisRaw("P1_Horizontal");
                break;
            case Player_ID.Two:
                value = Input.GetAxisRaw("P2_Horizontal");
                break;
            case Player_ID.Three:
                value = Input.GetAxisRaw("P3_Horizontal");
                break;
            case Player_ID.Four:
                value = Input.GetAxisRaw("P4_Horizontal");
                break;
        }
        return value;
    }

    public static bool get_Jump_Button(Player_ID player_ID, Button_Event_State button_Event_State)
    {
        var value = false;

        switch (player_ID)
        {
            case Player_ID.One:
                value = get_Button_Input("P1_Jump", button_Event_State);
                break;
            case Player_ID.Two:
                value = get_Button_Input("P2_Jump", button_Event_State);
                break;
            case Player_ID.Three:
                value = get_Button_Input("P3_Jump", button_Event_State);
                break;
            case Player_ID.Four:
                value = get_Button_Input("P4_Jump", button_Event_State);
                break;
        }
        return value;
    }

    public static bool get_Jump_Button(Button_Event_State button_Event_State)
    {
        if (get_Button_Input("P1_Jump", button_Event_State))
        {
            return true;
        }
        else if (get_Button_Input("P2_Jump", button_Event_State))
        {
            return true;
        }
        else if (get_Button_Input("P3_Jump", button_Event_State))
        {
            return true;
        }
        else if (get_Button_Input("P4_Jump", button_Event_State))
        {
            return true;
        }
        return false;
    }

    public static bool get_Interact_Button(Player_ID player_ID, Button_Event_State button_Event_State)
    {
        var value = false;

        switch (player_ID)
        {
            case Player_ID.One:
                value = get_Button_Input("P1_Interact", button_Event_State);
                break;
            case Player_ID.Two:
                value = get_Button_Input("P2_Interact", button_Event_State);
                break;
            case Player_ID.Three:
                value = get_Button_Input("P3_Interact", button_Event_State);
                break;
            case Player_ID.Four:
                value = get_Button_Input("P4_Interact", button_Event_State);
                break;
        }
        return value;
    }


    public static bool get_Interact_Button(Button_Event_State button_Event_State)
    {
        if (get_Button_Input("P1_Interact", button_Event_State))
        {
            return true;
        }
        else if (get_Button_Input("P2_Interact", button_Event_State))
        {
            return true;
        }
        else if (get_Button_Input("P3_Interact", button_Event_State))
        {
            return true;
        }
        else if (get_Button_Input("P4_Interact", button_Event_State))
        {
            return true;
        }
        return false;
    }


    private static bool get_Button_Input(string button_Label, Button_Event_State button_Event_State)
    {
        var value = false;

        switch (button_Event_State)
        {
            case Button_Event_State.Default: value = Input.GetButton(button_Label);
                break;
            case Button_Event_State.Down: value = Input.GetButtonDown(button_Label);
                break;
            case Button_Event_State.Up: value = Input.GetButtonUp(button_Label);
                break;
        }
        return value;
    }

}



