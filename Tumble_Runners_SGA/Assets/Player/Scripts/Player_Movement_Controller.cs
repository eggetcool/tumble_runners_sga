﻿using UnityEngine;
using System;
using System.Collections;
using Player_Collection;
using Utility_Collection;

[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(CircleCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]


public class Player_Movement_Controller : Player_Controller
{

    public float ground_Speed;
    public float acceleration;
    public float jump_Force;
    public float air_Jump_Force;
    public float jump_Force_Forward;
    [Range(0.1f, 0.9f)]
    public float torque_Inheritance_Percent;
    public float physics_Cast_Offset;
    public LayerMask ground_Layer_Mask;

    private Transform m_Transform;
    private Rigidbody2D m_Rigidbody;
    private BoxCollider2D m_Box_Collider;
    private CircleCollider2D m_Circle_Collider;
    private Player_Graphic_Controller m_Graphic_Controller;
    private Game_Controller m_Game_Controller;

    private float m_Current_Speed;
    private float m_Player_Total_Mass;
    private float[] m_Physics_Info;
    public bool m_Is_Grounded, m_In_Water, m_Accelerating;
    private bool[] m_Jumping;
    private bool[] m_Air_Jumping;

    private AnimationCurve m_Acceleration_Curve;
    private Vector2 m_Ground_Contact_Point;
    private Player_ID m_Player_ID;
    private Player_Direction m_Player_Direction;

    private bool[] m_Trowing;
    private float m_Trow_Vertical_Force;

    public float Vertical_Velocity
    {
        get {
            var local_Velocity = m_Transform.InverseTransformDirection(m_Rigidbody.velocity);
            return local_Velocity.y;
        }
    }
    public bool Is_Grounded { get { return m_Is_Grounded; } }
    public Vector2 Ground_Contact_Point { get { return m_Ground_Contact_Point; } }
    public float Ground_Distance { get { return (Mathf.Abs(m_Circle_Collider.offset.y) + m_Circle_Collider.radius) * m_Transform.lossyScale.y; } }
    public bool In_Water { get { return m_In_Water; } }
    public bool Reached_Max_Speed { get { return (m_Current_Speed == ground_Speed); } }


    private void Awake ()
    {
        m_Transform = GetComponent<Transform>();
        m_Rigidbody = m_Transform.GetComponent<Rigidbody2D>();
        m_Box_Collider = m_Transform.GetComponent<BoxCollider2D>();
        m_Circle_Collider = m_Transform.GetComponent<CircleCollider2D>();
        m_Graphic_Controller = m_Transform.GetComponent<Player_Graphic_Controller>();
        m_Physics_Info = new float[2];
        m_Jumping = new bool[2];
        m_Air_Jumping = new bool[2];
        m_Trowing = new bool[2];
    }


    private void Start ()
    {
        m_Rigidbody.freezeRotation = true;
        m_Rigidbody.interpolation = RigidbodyInterpolation2D.Interpolate;
        m_Rigidbody.WakeUp();

        Keyframe[] key_Points = new Keyframe[2];
        key_Points[0] = new Keyframe(0.0f, 0.0f);
        key_Points[0].outTangent = 12.0f;
        key_Points[1] = new Keyframe(1.0f, ground_Speed);

        m_Acceleration_Curve = new AnimationCurve(key_Points);

        m_Physics_Info[0] = m_Circle_Collider.radius * m_Transform.lossyScale.x;
        m_Physics_Info[1] = Mathf.Abs(m_Circle_Collider.offset.y) * m_Transform.lossyScale.y;
        //float mass = m_Transform.GetComponentInChildren<Ragdoll_Controller2D>().body_Mass;
        m_Player_Total_Mass = m_Rigidbody.mass;
        m_Player_Direction = Player_Direction.Right;
    }

    
    private void Update ()
    {
        if (get_Jump_Button(m_Player_ID, Button_Event_State.Down))
        {
            if (m_Is_Grounded)
            {
                m_Jumping[0] = true;
                m_Transform.SendMessage("On_Player_Jump", SendMessageOptions.RequireReceiver);
            }
            else if (!m_Is_Grounded && !m_Air_Jumping[1])
            {
                m_Transform.SendMessage("On_Player_Double_Jump", SendMessageOptions.RequireReceiver);
                m_Air_Jumping[0] = true;
            }
        }

        float axis_Value = get_Horizontal_Axis(m_Player_ID);

        if (!Mathf.Approximately(axis_Value, 0.0f))
        {
            Player_Direction player_Direction = (axis_Value < 0.0f) ? Player_Direction.Left :
                Player_Direction.Right;

            if (player_Direction != m_Player_Direction)
            {
                m_Player_Direction = player_Direction;
                Vector2 offset = m_Box_Collider.offset;
                offset.x = -offset.x;
                m_Box_Collider.offset = offset;
                m_Graphic_Controller.On_Direction_Change(m_Player_Direction);
            }

            //if (!m_Accelerating && !Mathf.Approximately(m_Current_Speed, ground_Speed))
            //{
            //    StartCoroutine(Acceleration());
            //}
        }
        //if (m_Is_Grounded && Mathf.Approximately(get_Horizontal_Axis(m_Player_ID), 0.0f))
        //{
        //    m_Current_Speed = 0.0f;
        //}

        m_Transform.rotation = Math_Ex.get_Look_Rotation(m_Transform.position, m_Game_Controller.Track_Origin);
    }


    private void FixedUpdate ()
    {
        Ground_Physics_Cast(m_Is_Grounded);

        if (m_Trowing[0])
        {
            float force = Math_Ex.get_Jump_Force_Translation(m_Trow_Vertical_Force * m_Player_Total_Mass, m_Rigidbody.gravityScale);
            Vector2 direction = (Vector2)m_Transform.up;
            m_Rigidbody.AddForce(direction * force, ForceMode2D.Impulse);
            m_Trowing[1] = true;
            m_Trowing[0] = false;
        }

        else if (!m_Trowing[1])
        {
            if (m_Jumping[0])
            {
                float force = Math_Ex.get_Jump_Force_Translation(jump_Force * m_Player_Total_Mass, m_Rigidbody.gravityScale);
                float forward_Force = Math_Ex.get_Jump_Force_Translation(jump_Force_Forward * m_Player_Total_Mass, m_Rigidbody.gravityScale);
                Vector2 direction = (Vector2)m_Transform.up;
                m_Rigidbody.AddForce(Vector2.up * force, ForceMode2D.Impulse);
                m_Rigidbody.AddForce(m_Transform.forward * forward_Force * (int)m_Player_Direction, ForceMode2D.Impulse);
                m_Jumping[0] = false;
                m_Jumping[1] = true;
            }
            else if (m_Air_Jumping[0])
            {
                float force = Math_Ex.get_Jump_Force_Translation(jump_Force * m_Player_Total_Mass, m_Rigidbody.gravityScale);
                float forward_Force = Math_Ex.get_Jump_Force_Translation(jump_Force_Forward * m_Player_Total_Mass, m_Rigidbody.gravityScale);
                Vector2 direction = (Vector2)m_Transform.up;
                Vector2 local_Vel = m_Transform.InverseTransformDirection(m_Rigidbody.velocity);
                local_Vel.y = 0.0f;
                m_Rigidbody.velocity = m_Transform.TransformDirection(local_Vel);
                m_Rigidbody.AddForce(Vector2.up * force, ForceMode2D.Impulse);
                m_Rigidbody.AddForce(m_Transform.forward * forward_Force * (int)m_Player_Direction, ForceMode2D.Impulse);
                m_Air_Jumping[0] = false;
                m_Air_Jumping[1] = true;
            }
            else if (m_Is_Grounded)
            {
                m_Rigidbody.velocity = Movement_Velocity(m_Player_Direction, m_Rigidbody.velocity, false);
                //Vector2 added_Force = Centrifugal_Force(m_Game_Controller.Track_Torque, m_Transform.position, m_Game_Controller.Track_Origin);
                //m_Rigidbody.AddForce(added_Force, ForceMode2D.Force);
            }
            //else
            //{
            //    m_Rigidbody.velocity = Movement_Velocity(m_Player_Direction, m_Rigidbody.velocity, false);
            //}

        }
    }


    private void OnTriggerEnter2D (Collider2D collider)
    {
        if (collider.tag == "Waterfall")
            m_In_Water = true;
    }


    private void OnTriggerExit2D (Collider2D collider)
    {
        if (collider.tag == "Waterfall")
            m_In_Water = false;
    }


    private void Ground_Physics_Cast (bool prev_Grounded)
    {
        float distance = m_Physics_Info[1] + m_Physics_Info[0] + physics_Cast_Offset;
        Vector2 origin = m_Transform.position;

        RaycastHit2D hit_Info = Physics2D.CircleCast(origin, m_Physics_Info[0], -m_Transform.up, distance, ground_Layer_Mask);
        
        if (hit_Info.collider != null)
        {
            m_Is_Grounded = true;
            m_Ground_Contact_Point = hit_Info.point;
        }
        else
            m_Is_Grounded = false;

        if (m_Is_Grounded && !prev_Grounded)
        {
            m_Jumping[1] = false;
            m_Air_Jumping[1] = false;
            m_Trowing[1] = false;
            m_Rigidbody.velocity = Cancel_Impact_Velocity(m_Rigidbody.velocity);
        }
    }


    private Vector2 Cancel_Impact_Velocity (Vector2 current_Velocity)
    {
        var local_Vel = m_Transform.InverseTransformDirection(current_Velocity);
        local_Vel.y = 0.0f;
        return m_Transform.TransformDirection(local_Vel);
    }


    private Vector2 Movement_Velocity(Player_Direction player_Direction, Vector2 velocity, bool grounded)
    {
        var current_Velocity_Y = m_Transform.InverseTransformDirection(velocity).y;
        var direction = Vector2.right * get_Horizontal_Axis(m_Player_ID);
        var added_Track_Torque = (grounded) ? -m_Game_Controller.Track_Torque * torque_Inheritance_Percent : 0.0f;
        direction.x *= ground_Speed;
        direction.y = current_Velocity_Y;
        return m_Transform.TransformDirection(direction.x + added_Track_Torque, direction.y, 0.0f);
    }


    private Vector2 Track_Velocity_Inheritance()
    {
        return m_Transform.TransformDirection(Vector2.right * m_Game_Controller.Track_Torque);
    }


    private Vector2 Centrifugal_Force(float torque, Vector2 position, Vector2 center_Point)
    {
        var normal = (position - center_Point).normalized;
        return normal * -torque;
    }


    private IEnumerator Acceleration()
    {
        float acceleration_Value = 0.0f;
        m_Accelerating = true;

        while (acceleration_Value < 1.0f && m_Accelerating)
        {
            acceleration_Value = Math_Ex.Const_Lerp(acceleration_Value, 1.0f, acceleration, Time.deltaTime);
            m_Current_Speed = m_Acceleration_Curve.Evaluate(acceleration_Value);

            if (Mathf.Approximately(acceleration_Value, 1.0f))
            {
                m_Current_Speed = ground_Speed;
                break;
            }
            else if (Mathf.Approximately(get_Horizontal_Axis(m_Player_ID), 0.0f))
            {
                m_Current_Speed = 0.0f;
                break;
            }
            else if (m_Jumping[1] || !m_Is_Grounded)
            {
                break;
            }
            yield return null;
        }

        m_Accelerating = false;
    }


    public void On_Player_Throw (float vertical_Force)
    {
        m_Trowing[0] = true;
        m_Trow_Vertical_Force = vertical_Force;
    }


    public void Initialize_Player(Game_Controller game_Controller)
    {
        m_Game_Controller = game_Controller;
    }


    public void Initialize_Player_ID(Player_ID player_ID)
    {
        m_Player_ID = player_ID;
    }


}