﻿using UnityEngine;
using System;
using System.Collections;
using Player_Collection;
using Utility_Collection;


public class Player_Graphic_Controller : Player_Controller
{

    private Transform m_Transform;
    private Transform m_Fire_Cracker_Transform;
    private Animator m_Animator;
    public Animator m_Effect_Animator;
    private SpriteRenderer m_Sprite_Renderer;
    private Player_Movement_Controller m_Movement_Controller;
    private Player_ID m_Player_ID;
    private Player_Direction m_Player_Direction;

    public Player_ID get_Player_ID { get { return m_Player_ID; } }
    public Player_Direction get_Player_Direction { get { return m_Player_Direction; } }


    private void Awake ()
    {
        m_Transform = GetComponent<Transform>();
        m_Animator = m_Transform.GetComponentInChildren<Animator>();
        m_Sprite_Renderer = m_Transform.GetComponent<SpriteRenderer>();
        m_Movement_Controller = m_Transform.GetComponent<Player_Movement_Controller>();
        m_Effect_Animator = m_Transform.FindChild("Run_Trail").GetComponent<Animator>();
        m_Fire_Cracker_Transform = m_Transform.FindChild("Fire_Cracker");
    }


    private void Start ()
    {
        m_Animator.runtimeAnimatorController = get_Animator_Controller(m_Player_ID);
    }


    private void FixedUpdate ()
    {
        float axis_Value = Mathf.Abs(get_Horizontal_Axis(m_Player_ID));
        m_Animator.SetFloat("Axis_Value", axis_Value);
        m_Animator.SetFloat("Vertical_Vel", m_Movement_Controller.Vertical_Velocity);
        m_Animator.SetBool("Grounded", m_Movement_Controller.Is_Grounded);

        m_Effect_Animator.SetFloat("Axis_Value", axis_Value);
        m_Effect_Animator.SetBool("Grounded", m_Movement_Controller.Is_Grounded);
        m_Effect_Animator.SetBool("Max_Speed", m_Movement_Controller.Reached_Max_Speed);
    }


    private RuntimeAnimatorController get_Animator_Controller(Player_ID player_ID)
    {
        RuntimeAnimatorController anim_Controller = new RuntimeAnimatorController();

        switch (player_ID)
        {
            case Player_ID.One:
                anim_Controller = (RuntimeAnimatorController)Resources.Load(
                    "Player/Character_Animator_Red",
                    typeof(RuntimeAnimatorController)
                );
                break;
            case Player_ID.Two:
                anim_Controller = (RuntimeAnimatorController)Resources.Load(
                    "Player/Character_Animator_Green",
                    typeof(RuntimeAnimatorController)
                );
                break;
            case Player_ID.Three:
                anim_Controller = (RuntimeAnimatorController)Resources.Load(
                    "Player/Character_Animator_Blue",
                    typeof(RuntimeAnimatorController)
                );
                break;
            case Player_ID.Four:
                anim_Controller = (RuntimeAnimatorController)Resources.Load(
                    "Player/Character_Animator_Orange",
                    typeof(RuntimeAnimatorController)
                );
                break;
        }

        return anim_Controller;
    }


    public void On_Direction_Change(Player_Direction new_Direction)
    {
        m_Sprite_Renderer.flipX = (new_Direction == Player_Direction.Left);
        m_Player_Direction = new_Direction;
        Vector3 new_Pos = m_Fire_Cracker_Transform.position;
        new_Pos.x = -new_Pos.x;
    }


    public void Initialize_Player_ID(Player_ID player_ID)
    {
        m_Player_ID = player_ID;
    }


    public void On_Player_Jump ()
    {
        Transform _object = Instantiate(Resources.Load("Player/Jump_Smoke", typeof(Transform))) as Transform;
        _object.position = m_Movement_Controller.Ground_Contact_Point;
        _object.rotation = m_Transform.rotation;
    }


    public void On_Player_Double_Jump()
    {
        m_Animator.SetBool("Double_Jump", true);
    }


    public void Activate_Fire_Cracker (bool value)
    {
        m_Fire_Cracker_Transform.gameObject.SetActive(value);
    }


}



