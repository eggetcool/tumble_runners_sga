﻿using UnityEngine;
using System.Collections;

public class Menu_Animator_Controller : StateMachineBehaviour
{

    private Menu_Sock_Movement m_SockMovement;


    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (m_SockMovement == null)
        {
            m_SockMovement = animator.transform.root.GetComponent<Menu_Sock_Movement>();
        }

        if (stateInfo.IsTag("State_Idle"))
        {
            animator.SetBool("Start_Idle", false);
        }
        else if (stateInfo.IsTag("State_Sprint"))
        {
            animator.SetBool("Start_Sprint", false);
        }
        else if (stateInfo.IsTag("State_Jump"))
        {
            animator.SetBool("Start_Jump", false);
        }
    }


    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (stateInfo.IsTag("State_Idle") && !animator.GetBool("Start_Sprint"))
        {
            if (m_SockMovement.get_moving)
            {
                animator.SetBool("Start_Sprint", true);
            }
        }
        else if (stateInfo.IsTag("State_Sprint") && !animator.GetBool("Start_Idle"))
        {
            if (!m_SockMovement.get_moving)
            {
                animator.SetBool("Start_Idle", true);
            }
        }
        else if (stateInfo.IsTag("State_Idle") && !animator.GetBool("Start_Jump"))
        {
            if (!m_SockMovement.get_moving && m_SockMovement.get_jumping)
            {
                animator.SetBool("Start_Jump", true);
            }
        }
    }


    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }


}

