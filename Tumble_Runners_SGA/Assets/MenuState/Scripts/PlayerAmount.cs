﻿using UnityEngine;
using System.Collections;

public class PlayerAmount : MonoBehaviour {

    private MenuChoice m_MenuChoice;
    private Transform m_MenuBack;

    public int m_PlayerAmount;
    public bool[] m_Active;
	// Use this for initialization
	void Start ()
    {
        m_Active = new bool[4];

        m_MenuBack = GameObject.Find("tumble dryer").transform;

        if (m_MenuChoice == null)
        {
            m_MenuChoice = m_MenuBack.GetComponent<MenuChoice>();
        }

    }
	
    void Awake ()
    {
        DontDestroyOnLoad(transform.gameObject);
    }

	// Update is called once per frame
	void Update ()
    {
        m_PlayerAmount = m_MenuChoice.m_NrPlayers;
        m_Active[0] = m_MenuChoice.m_isActive[0];
        m_Active[1] = m_MenuChoice.m_isActive[1];
        m_Active[2] = m_MenuChoice.m_isActive[2];
        m_Active[3] = m_MenuChoice.m_isActive[3];
    }
}
