﻿using UnityEngine;
using System.Collections;

public class JoinAnimationBehaviour : StateMachineBehaviour
{

    private JoinScript m_JoinScript;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (m_JoinScript == null)
        {
            m_JoinScript = animator.transform.root.GetComponent<JoinScript>();
        }

        if (stateInfo.IsTag("State_Join"))
        {
            animator.SetBool("PlayerExists", false);
        }
        else if (stateInfo.IsTag("State_Leave"))
        {
            animator.SetBool("PlayerExists", true);
        }
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (stateInfo.IsTag("State_Join") && !animator.GetBool("PlayerExists"))
        {
            if (m_JoinScript.m_isActive)
            {
                animator.SetBool("PlayerExists", true);
            }
        }
        else if (stateInfo.IsTag("State_Leave") && animator.GetBool("PlayerExists"))
        {
            if (!m_JoinScript.m_isActive)
            {
                animator.SetBool("PlayerExists", false);
            }
        }
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }
}
