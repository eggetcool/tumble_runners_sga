﻿using UnityEngine;
using System.Collections;

public class JoinScript : MonoBehaviour {

    private MenuChoice m_menuChoice;
    public bool m_isActive;

    // Use this for initialization
    void Start ()
    {
        m_menuChoice = GameObject.Find("tumble dryer").GetComponent<MenuChoice>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (transform.position.y == 1.2f)
            m_isActive = m_menuChoice.m_isActive[0];
        else if (transform.position.y == 0.8f)
            m_isActive = m_menuChoice.m_isActive[1];
        else if (transform.position.y == 0.4f)
            m_isActive = m_menuChoice.m_isActive[2];
        else if (transform.position.y == 0)
            m_isActive = m_menuChoice.m_isActive[3];
    }
}
