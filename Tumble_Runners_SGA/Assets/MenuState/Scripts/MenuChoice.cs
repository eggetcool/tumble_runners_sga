﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Player_Collection;
using Utility_Collection;

public class MenuChoice : Player_Controller
{
    public bool get_starting { get { return m_startJump; } }
    public bool get_startRun { get { return m_StartRun; } }
    public float get_playerNr { get { return m_NrPlayers; } }

    public bool m_isStarting, m_StartRun, m_startJump, m_hasJumped;
    public bool[] m_isActive;

    public int m_NrPlayers;
    public GameObject m_Sock1;
    public GameObject m_Sock2;
    public GameObject m_Sock3;
    public GameObject m_Sock4;

    public Transform m_Fade;
    public Transform m_FadeClone;

    public Game_Scene_Data m_GameData;

    private float m_Delay;
    
    private GameObject m_Sock1Clone;
    private GameObject m_Sock2Clone;
    private GameObject m_Sock3Clone;
    private GameObject m_Sock4Clone;

    private float m_alpha = 0;

    public Text m_text;
    
    void Start()
    {
        m_isActive = new bool[4];
        m_text = FindObjectOfType<Text>();
        GameObject data_Object = new GameObject();
        data_Object.tag = "Load_Game_Data";
        m_GameData = data_Object.AddComponent<Game_Scene_Data>();
    }

    void Update()
    {
        if (get_Jump_Button(Player_ID.One, Button_Event_State.Down) && !m_isActive[0])//Creates player one
        {
            m_NrPlayers++;
            m_Sock1Clone = Instantiate(m_Sock1, new Vector3(-6.5f, -3, 0), Quaternion.identity) as GameObject;
            m_Sock1Clone.GetComponent<Menu_Sock_Movement>().enabled = true;
            m_Sock1Clone.transform.localScale -= new Vector3(0.4F, 0.4F, 0);
            m_isActive[0] = true;
            m_GameData.activated_Players[0] = m_isActive[0];
        }
        if (get_Jump_Button(Player_ID.Two, Button_Event_State.Down) && !m_isActive[1])//Creates player two
        {
            m_NrPlayers++;
            m_Sock2Clone = Instantiate(m_Sock2, new Vector3(-6.5f, -3, 0), Quaternion.identity) as GameObject;
            m_Sock2Clone.GetComponent<Menu_Sock_Movement>().enabled = true;
            m_Sock2Clone.transform.localScale -= new Vector3(0.4F, 0.4F, 0);
            m_isActive[1] = true;
            m_GameData.activated_Players[1] = m_isActive[1];
        }
        if (get_Jump_Button(Player_ID.Three, Button_Event_State.Down) && !m_isActive[2])//Creates player three
        {
            m_NrPlayers++;
            m_Sock3Clone = Instantiate(m_Sock3, new Vector3(-6.5f, -3, 0), Quaternion.identity) as GameObject;
            m_Sock3Clone.GetComponent<Menu_Sock_Movement>().enabled = true;
            m_Sock3Clone.transform.localScale -= new Vector3(0.4F, 0.4F, 0);
            m_isActive[2] = true;
            m_GameData.activated_Players[2] = m_isActive[2];
        }
        if (get_Jump_Button(Player_ID.Four, Button_Event_State.Down) && !m_isActive[3])//Creates player four
        {
            m_NrPlayers++;
            m_Sock4Clone = Instantiate(m_Sock4, new Vector3(-6.5f, -3, 0), Quaternion.identity) as GameObject;
            m_Sock4Clone.GetComponent<Menu_Sock_Movement>().enabled = true;
            m_Sock4Clone.transform.localScale -= new Vector3(0.4F, 0.4F, 0);
            m_isActive[3] = true;
            m_GameData.activated_Players[3] = m_isActive[3];
        }


        if (get_Interact_Button(Player_ID.Four, Button_Event_State.Down) && m_isActive[3])//Destroys player four
        {
            
            Destroy(m_Sock4Clone);
            m_NrPlayers--;
            m_isActive[3] = false;
            m_GameData.activated_Players[3] = m_isActive[3];
        }
        if (get_Interact_Button(Player_ID.Three, Button_Event_State.Down) && m_isActive[2])//Destroys player three
        {
            Destroy(m_Sock3Clone);
            m_NrPlayers--;
            m_isActive[2] = false;
            m_GameData.activated_Players[2] = m_isActive[2];
        }
        if (get_Interact_Button(Player_ID.Two, Button_Event_State.Down) && m_isActive[1])//Destroys player two
        {
            Destroy(m_Sock2Clone);
            m_NrPlayers--;
            m_isActive[1] = false;
            m_GameData.activated_Players[1] = m_isActive[1];
        }
        if (get_Interact_Button(Player_ID.One, Button_Event_State.Down) && m_isActive[0])//Destroys player two
        {
            Destroy(m_Sock1Clone);
            m_NrPlayers--;
            m_isActive[0] = false;
            m_GameData.activated_Players[0] = m_isActive[0];
        }

        if (m_NrPlayers > 1)
            m_isStarting = true;//Starts the transition to playstate
        else
        {
            m_isStarting = false;
            m_text.text = "";
            m_Delay = 0;
        }
            
    }
    
    void FixedUpdate()
    {

        if (m_startJump && !m_hasJumped)
        {
            transform.position = new Vector3(transform.position.x - 0.35f, transform.position.y, transform.position.z);//offset because the animation isn centered
            m_FadeClone = Instantiate(m_Fade, new Vector3(0, 0, 0), Quaternion.identity) as Transform;//Instantiates fade
            m_hasJumped = true;
        }

        if (m_isStarting)
        {
            m_Delay += Time.deltaTime;
            m_text.text = (Mathf.RoundToInt(10 - m_Delay)).ToString();
        }
        
        if (Mathf.RoundToInt(10 - m_Delay) < 1)
            m_text.text = "";

        if(m_Delay >= 9.5f)
            m_startJump = true;//Tumbler jumps forward

        if (m_Delay >= 10f)
            m_StartRun = true;//Players start running towards the tumblr

        if(m_Delay >= 11.6f)
        {
            float m_camSize = Camera.main.orthographicSize;//Camera zoom
            m_camSize -= 0.02f;//
            Camera.main.orthographicSize = m_camSize;//
            if (Camera.main.transform.position.y > -1.5f)//
                Camera.main.transform.position = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y - 0.015f, Camera.main.transform.position.z);//
        }
        if (m_Delay >= 11.7f)
        {
            Music_Script.m_instance.m_fadeSpeed = 0.01f;
            Music_Script.m_instance.m_fade = true;
            m_alpha += 0.01f;//Fade out starts
            m_FadeClone.GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, m_alpha);
        }
        if (m_Delay >= 13.7f)
        {
            SceneManager.LoadScene("Game_Scene");//Next scene
        }
        /*
        if (Input.GetKeyDown(KeyCode.Return))
        {
            SceneManager.LoadScene(1);
        }*/
    }
}