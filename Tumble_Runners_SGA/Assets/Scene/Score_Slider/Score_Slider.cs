﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Player_Collection;


public class Score_Slider : MonoBehaviour
{
    public Player_ID player_ID_Reference;
    public Color set_Slider_Overlay_Color
    {
        set
        {
            m_Change_Color = true;
            m_Slider_Overlay_Color = value;
        }
    }
    public float get_Fill_Amount { get { return m_Slider_Image.fillAmount; } }

    private Color m_Slider_Overlay_Color;
    private Image m_Slider_Image;
    private Image m_Overlay_Image;
    private bool m_Change_Color;
    private bool m_Coin_Animation_Start;
    private float m_Coin_Timer;
    private GameObject m_Dirt_1;
    private GameObject m_Dirt_2;
    private GameObject m_Dirt_3;
    private GameObject m_Clean;
    private GameObject m_Coin_Feedback;
    private float m_Coin_Score;
    private float m_Water_Score;

    ////////
    private GameObject m_Ray_Light_Object;
    private GameObject m_Crown_1;
    private GameObject m_Crown_2;
    private GameObject m_Crown_3;
    private GameObject m_Crown_4;
    private GameObject m_Crown_5;
    private GameObject m_Foam;
    private GameObject m_Sponge;
    private float m_Ray_Of_Light_Timer;
    private bool m_Ray_Of_Light_Bool;
    private bool m_Single_Use_Bool_For_Ray = true;
    private float m_Threshold;
    ////////

    private void Awake()
    {
        m_Slider_Image = transform.FindChild("Slider").GetComponent<Image>();
        m_Overlay_Image = transform.FindChild("Overlay").GetComponent<Image>();
    }


    private void Start()
    {
        m_Slider_Image.fillAmount = 0.0f;
        m_Coin_Score = 0.0f;
        m_Water_Score = 0.0f;
        m_Threshold = 0.2f;

        m_Overlay_Image.color = m_Slider_Overlay_Color;

        m_Dirt_1 = transform.FindChild("Overlay_Dirt_Level1").gameObject;
        m_Dirt_2 = transform.FindChild("Overlay_Dirt_Level2").gameObject;
        m_Dirt_3 = transform.FindChild("Overlay_Dirt_Level3").gameObject;
        m_Clean = transform.FindChild("Overlay_Clean").gameObject;
        

        m_Coin_Feedback = transform.FindChild("Coin_Feedback").gameObject;
        ////////
        m_Ray_Light_Object = transform.FindChild("Ray_Light").gameObject;
        m_Ray_Of_Light_Timer = 0.0f;

        m_Crown_1 = transform.FindChild("Crown_1").gameObject;
        m_Crown_2 = transform.FindChild("Crown_2").gameObject;
        m_Crown_3 = transform.FindChild("Crown_3").gameObject;
        m_Crown_4 = transform.FindChild("Crown_4").gameObject;
        m_Crown_5 = transform.FindChild("Crown_5").gameObject;
        m_Foam = transform.FindChild("HUD_Foam").gameObject;
        m_Sponge = transform.FindChild("HUD_Sponge").gameObject;
        ///////
    }


    private void Update()
    {
        if (m_Change_Color)
        {
            m_Overlay_Image.color = m_Slider_Overlay_Color;
            m_Change_Color = false;
        }

        if(m_Water_Score >= 0.0f && m_Water_Score < 0.8f)
        {
            m_Clean.SetActive(false);
        }

        if (m_Water_Score > 0.2f && m_Water_Score < 0.4f)
        {
            m_Dirt_1.SetActive(false);
            m_Dirt_2.SetActive(true);
        }

        else if (m_Water_Score > 0.4f && m_Water_Score < 0.6f)
        {
            m_Dirt_2.SetActive(false);
            m_Dirt_3.SetActive(true);
        }

        else if (m_Water_Score > 0.6f && m_Water_Score < 0.8f)
            m_Dirt_3.SetActive(false);

        else if (m_Water_Score > 0.8f)
            m_Clean.SetActive(true);



        /////// Fixa Ray of Light

        if (m_Coin_Score >= 0.0f && m_Coin_Score < 0.8f)
        {
            m_Crown_5.SetActive(false);
        }

        if (m_Coin_Score >= 0.0f && m_Coin_Score <= 0.2f)
        {
            m_Crown_1.SetActive(true);
        }

        if (m_Coin_Score > 0.2f && m_Coin_Score < 0.4f)
        {
            m_Crown_1.SetActive(false);
            m_Crown_2.SetActive(true);
            m_Crown_3.SetActive(false);
            m_Crown_4.SetActive(false);
            m_Crown_5.SetActive(false);
        }
        else if (m_Coin_Score > 0.4f && m_Coin_Score < 0.6f)
        {
            m_Crown_1.SetActive(false);
            m_Crown_2.SetActive(false);
            m_Crown_3.SetActive(true);
            m_Crown_4.SetActive(false);
            m_Crown_5.SetActive(false);
        }
        else if (m_Coin_Score > 0.6f && m_Coin_Score < 0.8f)
        {
            m_Crown_1.SetActive(false);
            m_Crown_2.SetActive(false);
            m_Crown_3.SetActive(false);
            m_Crown_4.SetActive(true);
            m_Crown_5.SetActive(false);
        }
        else if (m_Coin_Score > 0.8f)
        {
            m_Crown_1.SetActive(false);
            m_Crown_2.SetActive(false);
            m_Crown_3.SetActive(false);
            m_Crown_4.SetActive(false);
            m_Crown_5.SetActive(true);
        }




        

        if (m_Coin_Score > m_Threshold && !m_Ray_Light_Object.activeSelf)
        {
            m_Ray_Light_Object.SetActive(true);
            m_Threshold += 0.2f;
        }

        if (m_Ray_Light_Object.activeSelf)
            m_Ray_Of_Light_Bool = true;

        if (m_Ray_Of_Light_Bool)
        {
            m_Ray_Of_Light_Timer += Time.deltaTime;
            if (m_Ray_Of_Light_Timer > 0.75f)
            {
                m_Ray_Light_Object.SetActive(false);
                m_Ray_Of_Light_Bool = false;
            }
        }
        else
            m_Ray_Of_Light_Timer = 0.0f;
        ///////





        if (m_Coin_Animation_Start)
        {
            m_Coin_Feedback.SetActive(true);

            m_Coin_Timer += Time.deltaTime;

            if (m_Coin_Timer > 0.389f)
            {
                m_Coin_Feedback.SetActive(false);
                m_Coin_Animation_Start = false;
                m_Coin_Timer = 0.0f;
            }
        }
        else
            m_Coin_Feedback.SetActive(false);


    }


    public void On_Value_Change(float new_Value)
    {
        m_Slider_Image.fillAmount = new_Value;
    }

    public void On_Waterfall_Point(float new_Value, float prevFill)
    {
        m_Water_Score += new_Value;
        On_Value_Change(new_Value + prevFill);
    }

    public void On_Coin_Pickup(float new_Value, float prevFill)
    {
        m_Coin_Score += new_Value;
        On_Value_Change(new_Value + prevFill);
    }


    public void On_Pickup_Present()
    {
        m_Coin_Animation_Start = true;
    }

    public void Foam_Active(bool isActive)
    {
        m_Foam.SetActive(isActive);
    }

    public void Sponge_Active(bool isActive)
    {
        m_Sponge.SetActive(isActive);
    }
}



