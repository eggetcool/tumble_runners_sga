﻿using UnityEngine;
using System;
using System.Collections;
using Player_Collection;
using Utility_Collection;
using UnityEngine.SceneManagement;

public class Victory_Screen_Placement : Player_Controller
{
    private Player_ID[] m_Sorted_ID_List;
    private Player_ID m_Player_ID;          //For pressing button to get to Menu.
    private Transform m_Player_Transform;

    //TAG: Load_Win_Data
    void Start ()
    {
        GameObject Win_Data_Object = GameObject.FindGameObjectWithTag("Load_Win_Data");
        m_Sorted_ID_List = Win_Data_Object.GetComponent<Win_Scene_Data>().m_Player_Win_Rank;
        Destroy(Win_Data_Object);

        float offset = 1.39f;

        for (int i = 0; i < m_Sorted_ID_List.Length; i++)
        {
            int playerID = (int)m_Sorted_ID_List[i];

            if(playerID == (int)m_Sorted_ID_List[0])
                m_Player_Transform = Instantiate(Resources.Load("Victory/Victory_" + (playerID + 1).ToString(), typeof(Transform))) as Transform;

            else if(playerID == (int)m_Sorted_ID_List[m_Sorted_ID_List.Length - 1])
                m_Player_Transform = Instantiate(Resources.Load("Victory/Last_" + (playerID + 1).ToString(), typeof(Transform))) as Transform;

            else
                m_Player_Transform = Instantiate(Resources.Load("Victory/Clap_" + (playerID + 1).ToString(), typeof(Transform))) as Transform;


            GameObject m_Placement = GameObject.Find("Place_" + (i + 1).ToString());
            
            //This is for the offset of the looser animation.
            if (playerID == (int)m_Sorted_ID_List[m_Sorted_ID_List.Length - 1])
            {
                float valueY = m_Placement.transform.position.y - offset;
                Vector3 valueV = new Vector3(m_Placement.transform.position.x, valueY);
                m_Player_Transform.position = valueV;
            }
            else
                m_Player_Transform.position = m_Placement.transform.position;

        }
	}
    
    void FixedUpdate()
    {
        if(get_Interact_Button(Button_Event_State.Down))
            SceneManager.LoadScene("MenuScene");
    }
}
