﻿using UnityEngine;
using System.Collections;

public class KonfettiSound : MonoBehaviour
{

    public AudioClip m_KonfettiBoom;
    public AudioSource source;

    private float m_delay = 0;
    private bool m_soundPlayed;

    void Awake()
    {
        //source = GameObject.Find("KonfettiAudio").
        source = GetComponent<AudioSource>();
    }

    void FixedUpdate()
    {
        m_delay += Time.deltaTime;

        if (m_delay > 2.5f && !m_soundPlayed)
        {
            source.PlayOneShot(m_KonfettiBoom);
            m_soundPlayed = true;
        }
        else
        {
        }
    }
}
