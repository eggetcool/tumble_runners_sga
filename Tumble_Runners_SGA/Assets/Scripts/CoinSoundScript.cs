﻿using UnityEngine;
using System.Collections;

public class CoinSoundScript : MonoBehaviour {

    public AudioClip m_coinCrash;
    public AudioSource source;

    private float m_lowPitchRange = .8f;
    private float m_highPitchRange = 1f;
    private float m_velToVol = .2f;

    private float m_delay = 0;


    int x;

    void Awake ()
    {
        source = GameObject.Find("CoinAudio").GetComponent<AudioSource>();
        //GetComponent<AudioSource>();
        
    }

    void Update()
    {
        m_delay += Time.deltaTime;
    }

	void OnCollisionEnter2D (Collision2D hit)
    {
        if (hit.transform.gameObject.tag == "Player")
        {
            source.pitch = Random.Range(m_lowPitchRange, m_highPitchRange);
            float m_hitVol = hit.relativeVelocity.magnitude * m_velToVol;
            source.PlayOneShot(m_coinCrash, m_hitVol);

        }
        else
        {
        }
	}
}
