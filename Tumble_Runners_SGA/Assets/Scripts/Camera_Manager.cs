﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Utility_Collection;
using Player_Collection;


public class Camera_Manager : MonoBehaviour
{

    public float icon_Padding;
    public Vector2 camera_Min_Position;
    public Vector2 camera_Max_Position;
    public float camera_Offset;
    public float camera_Movement_Speed;
    public Vector2 player_Size;
    public float icon_Movement_Speed;

    private Transform m_Transform;
    private Transform m_Camera_Transform;
    private List<Transform> m_Player_Transforms;
    private List<Player_Indicator_Controller> m_Player_Indicators;
    
    private Camera m_Camera;
    private bool m_Players_Loaded;
    private Vector3 m_Cam_Velocity, m_Min_Pos, m_Max_Pos;


    private void Awake ()
    {
        m_Transform = GetComponent<Transform>();
        m_Camera = m_Transform.GetComponentInChildren<Camera>();
        m_Camera_Transform = m_Camera.GetComponent<Transform>();
        m_Player_Transforms = new List<Transform>();
        m_Player_Indicators = new List<Player_Indicator_Controller>();
    }

    
    private void Start ()
    {
        m_Max_Pos = new Vector3(camera_Max_Position.x, camera_Max_Position.y, m_Camera_Transform.position.z);
        m_Min_Pos = new Vector3(camera_Min_Position.x, camera_Min_Position.y, m_Camera_Transform.position.z);
    }

    
    private void Update ()
    {
        if (!m_Players_Loaded)
            return;
        Vector3 current_Position = m_Camera_Transform.position;
        Vector3 target_Position = get_Target_Position(current_Position);
        Vector3 new_Pos =  Vector3.SmoothDamp(current_Position, target_Position, ref m_Cam_Velocity, camera_Movement_Speed);
        new_Pos = Math_Ex.Clamp_Vector3(new_Pos, m_Min_Pos, m_Max_Pos);

        m_Camera_Transform.position = new_Pos;

        for (int i = 0; i < m_Player_Indicators.Count; i++)
        {
            bool is_Out_Of_Screen = false;
            Vector2 current_Pos = m_Player_Indicators[i].transform.position;
            Vector2 target = get_Indicator_Position(m_Player_Transforms[i], m_Player_Indicators[i].transform, m_Camera, player_Size, ref is_Out_Of_Screen);
            Vector2 new_Position = Math_Ex.Const_Vctr_Lerp(current_Pos, target, icon_Movement_Speed, Time.deltaTime);
            m_Player_Indicators[i].Set_Active(is_Out_Of_Screen);
            m_Player_Indicators[i].transform.position = new_Position;
        }
    }


    private Vector3 get_Target_Position(Vector3 current_Position)
    {
        Vector2 min = m_Player_Transforms[0].position;
        Vector2 max = m_Player_Transforms[0].position;

        for (int i = 1; i < m_Player_Transforms.Count; i++)
        {
            if (m_Player_Transforms[i].position.x < min.x)
                min = m_Player_Transforms[i].position;
            if (m_Player_Transforms[i].position.x > min.x)
                max = m_Player_Transforms[i].position;
        }

        Vector2 normal = (max - min).normalized;
        float distance = Vector2.Distance(min, max) / 2.0f;

        Vector2 result = min + (normal * distance);
        result.y += camera_Offset;

        return new Vector3(result.x, result.y, current_Position.z);
    }


    private Vector2 get_Indicator_Position(Transform player, Transform indicator, Camera camera, Vector2 player_Size, ref bool is_Out_Of_screen)
    {
        Vector2 camera_Offset = get_Camera_Offset(camera);
        Vector2 min_Pos = ((Vector2)camera.transform.position - (camera_Offset / 2.0f)) - Vector2.one * icon_Padding;
        Vector2 max_Pos = ((Vector2)camera.transform.position + (camera_Offset / 2.0f)) + Vector2.one * icon_Padding;

        float min_Pos_X = camera.transform.position.x - ((camera_Offset.x / 2.0f) + player_Size.x);
        float max_Pos_X = camera.transform.position.x + ((camera_Offset.x / 2.0f) + player_Size.x);

        if (Math_Ex.In_Value_Range(player.position.x, min_Pos_X, max_Pos_X))
        {
            float min_Pos_Y = camera.transform.position.y - ((camera_Offset.y / 2.0f) + player_Size.y);
            float max_Pos_Y = camera.transform.position.y + ((camera_Offset.y / 2.0f) + player_Size.y);
            if (Math_Ex.In_Value_Range(player.position.y, min_Pos_Y, max_Pos_Y))
            {
                is_Out_Of_screen = false;
            }
            else
                is_Out_Of_screen = true;
        }
        else
            
            is_Out_Of_screen = true;

        return  Math_Ex.Clamp_Vector2(player.position, min_Pos, max_Pos);
    }


    public void Initialize(Transform[] player_Transforms)
    {
        for (int i = 0; i < player_Transforms.Length; i++)
        {
            if (player_Transforms[i] != null)
            {
                m_Player_Transforms.Add(player_Transforms[i]);
                Add_Icon(player_Transforms[i]);
            }
        }
        m_Players_Loaded = true;
    }


    private void Add_Icon(Transform player_Transform)
    {
        string path = "";
        Player_ID id_Ref = player_Transform.GetComponent<Player_Graphic_Controller>().get_Player_ID;

        switch (id_Ref)
        {
            case Player_ID.One:
                path = "Icon/Player_Indicator_P1";
                break;
            case Player_ID.Two:
                path = "Icon/Player_Indicator_P2";
                break;
            case Player_ID.Three:
                path = "Icon/Player_Indicator_P3";
                break;
            case Player_ID.Four:
                path = "Icon/Player_Indicator_P4";
                break;
        }

        Player_Indicator_Controller _object = Instantiate(Resources.Load(path, typeof(Player_Indicator_Controller))) as Player_Indicator_Controller;
        _object.Initialize(player_Transform);
        m_Player_Indicators.Add(_object);
    }


    private Vector2 get_Camera_Offset(Camera camera)
    {
        var height = 2f * camera.orthographicSize;
        var width = height * camera.aspect;
        return new Vector2(width, height);
    }


}



