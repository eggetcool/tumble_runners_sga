﻿using UnityEngine;
using System.Collections;

public class CenterOfMass : MonoBehaviour 
{
    public Vector2 mass;
    private Rigidbody2D body;
    private bool started = false;

	void Start () 
    {
        started = true;
        body = GetComponent<Rigidbody2D>();
	}

    void OnDrawGizmos()
    {
        if (started)
            return;

        if (body == null)
            body = GetComponent<Rigidbody2D>();

        body.centerOfMass = mass;

        if (body)
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawSphere(body.worldCenterOfMass, 0.05f);
        }   
    }
}
