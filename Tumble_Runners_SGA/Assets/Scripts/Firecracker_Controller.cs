﻿using UnityEngine;
using System.Collections;

public class Firecracker_Controller : MonoBehaviour
{

    public float fire_Cracker_Delay;
    public float fire_Cracker_Life_Time;
    public float explosion_Damage;
    public float fire_Cracker_Distance;
    public LayerMask player_Layer_Mask;

    private Transform m_Transform;
    private Transform m_Current_Player;
    private SpriteRenderer m_Sprite_Renderer;
    private Player_Manager m_Player_Manager;
    private float m_Delay_Timer, m_Fire_Cracker_Timer;
    private bool m_Game_Over, m_Players_Loaded, m_Active;


    private void Awake ()
    {
        m_Transform = GetComponent<Transform>();
        m_Player_Manager = m_Transform.GetComponent<Player_Manager>();
    }


    private void Start ()
    {
        m_Delay_Timer = fire_Cracker_Delay;
        m_Fire_Cracker_Timer = fire_Cracker_Life_Time;
    }

    
    private void Update ()
    {
        if (m_Game_Over || !m_Players_Loaded)
            return;

        if (m_Delay_Timer > 0.0f)
        {
            m_Delay_Timer -= Time.deltaTime;

            if (m_Delay_Timer <= 0.0f)
            {
                Transform leader = m_Player_Manager.get_Leading_Player;

                for (int i = 0; i < m_Player_Manager.get_Players.Length; i++)
                {
                    if (m_Player_Manager.get_Players[i] != null && m_Player_Manager.get_Players[i].gameObject.activeSelf)
                    {
                        if (m_Player_Manager.get_Players[i] == leader)
                        {
                            Firecracker_Object _object = m_Player_Manager.get_Players[i].FindChild("Fire_Cracker").GetComponent<Firecracker_Object>();
                            _object.Intialize(this, fire_Cracker_Distance, player_Layer_Mask);
                            m_Current_Player = leader;
                        }
                        Firecracker_Object Firecracker_object = m_Player_Manager.get_Players[i].FindChild("Fire_Cracker").GetComponent<Firecracker_Object>();
                        Firecracker_object.Animation_Start(); 
                    }
                }
                m_Delay_Timer = fire_Cracker_Delay;
                m_Fire_Cracker_Timer = fire_Cracker_Life_Time;
                m_Active = true;
            }
        }

        if (m_Active && m_Fire_Cracker_Timer > 0.0f)
        {
            m_Fire_Cracker_Timer -= Time.deltaTime;

            if (m_Fire_Cracker_Timer <= 0.0f)
            {
                m_Player_Manager.On_Score_Change(m_Current_Player, explosion_Damage);
                Firecracker_Object _object = m_Current_Player.FindChild("Fire_Cracker").GetComponent<Firecracker_Object>();
                _object.Deactivate();
            }
        }
    }


    public void On_Player_Change(Transform player_Transform)
    {
        Firecracker_Object _object = m_Current_Player.FindChild("Fire_Cracker").GetComponent<Firecracker_Object>();
        _object.Deactivate();
        _object = player_Transform.FindChild("Fire_Cracker").GetComponent<Firecracker_Object>();
        _object.Intialize(this, fire_Cracker_Distance, player_Layer_Mask);
        m_Current_Player = player_Transform;
    }


    public void On_Win_Event()
    {
        m_Game_Over = true;
    }


    public void On_Game_Start ()
    {
        m_Players_Loaded = true;
    }
}