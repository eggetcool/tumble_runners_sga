﻿using UnityEngine;
using System.Collections;

public class Firecracker_Object : MonoBehaviour
{

    private Transform m_Transform;
    private Transform m_Root_Transform;
    private SpriteRenderer m_Sprite_Renderer;
    private Animator m_Animator;
    private Firecracker_Controller m_Fire_Cracker_Controller;
    private Player_Graphic_Controller m_Graphics_Controller;
    private float m_Ray_Distance;
    private LayerMask m_Player_Layer_Mask;
    private bool m_Active;
    [SerializeField]
    private float m_Check_Delay;
    private float m_Timer;


    void Awake ()
    {
        m_Transform = GetComponent<Transform>();
        m_Root_Transform = m_Transform.root;
        m_Graphics_Controller = m_Root_Transform.GetComponent<Player_Graphic_Controller>();
        m_Sprite_Renderer = m_Transform.GetComponent<SpriteRenderer>();
        m_Animator = m_Transform.GetComponent<Animator>();
        m_Active = false;
    }

    
    void FixedUpdate ()
    {
        if (!m_Active)
            return;

        if (m_Timer > 0.0f)
        {
            m_Timer -= Time.deltaTime;
            return;
        }

        //Vector2 direction = m_Root_Transform.right * (int)m_Graphics_Controller.get_Player_Direction;
        //RaycastHit2D hit_Info = Physics2D.Raycast(m_Root_Transform.position, direction, m_Ray_Distance, m_Player_Layer_Mask);
        //Debug.DrawRay(m_Root_Transform.right, direction * m_Ray_Distance, Color.red);

        RaycastHit2D hit_Info = Physics2D.CircleCast(m_Root_Transform.position, m_Ray_Distance, Vector2.zero, 0.0f, m_Player_Layer_Mask);

        if (hit_Info.collider != null)
        {
            //Debug.DrawRay(m_Root_Transform.right, direction * hit_Info.distance, Color.red);
            m_Fire_Cracker_Controller.On_Player_Change(hit_Info.transform);
        }
    }


    public void Intialize (Firecracker_Controller controller, float ray_Distance, LayerMask player_Layer_Mask)
    {
        m_Fire_Cracker_Controller = controller;
        m_Ray_Distance = ray_Distance;
        m_Player_Layer_Mask = player_Layer_Mask;
        m_Active = true;
        m_Sprite_Renderer.enabled = true;
        m_Timer = m_Check_Delay;
    }


    public void Deactivate()
    {
        m_Active = false;
        m_Sprite_Renderer.enabled = false;
    }

    public void Animation_Start()
    {
        m_Animator.SetBool("Start", true);
    }


}



