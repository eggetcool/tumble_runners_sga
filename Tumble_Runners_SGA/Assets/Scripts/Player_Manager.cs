﻿using UnityEngine;
using System;
using System.Collections;
using Utility_Collection;
using Player_Collection;


public class Player_Manager : Player_Controller
{
    public float button_Hit_Fill_Amount;
    public float target_Water_Time;
    public Color[] slider_Overlay_Color;

    private Transform[] m_Spawn_Transforms;
    public Transform[] m_Player_Transforms;
    private Transform m_Lead_Ref_Point;
    private bool[] m_Players_In_Game;
    private Score_Slider[] m_Score_Sliders;
    private float[] m_Slider_Values;
    private Transform m_Transform;
    private Game_Controller m_Game_Controller;
    private Firecracker_Controller m_Fire_Cracker_Controller;
    private int m_Active_Player_Amounts;
    private bool m_Player_Spawned_In, m_Game_Over;

    public int Active_Player_Amount { get { return m_Active_Player_Amounts; } }
    public Transform get_Leading_Player
    {
        get
        {
            float value = 0.0f;
            int index = 0;

            for (int i = 0; i < m_Player_Transforms.Length; i++)
            {
                if (m_Player_Transforms[i] != null)
                {
                    int id_Ref = (int)m_Player_Transforms[i].GetComponent<Player_Graphic_Controller>().get_Player_ID;
                    if (m_Score_Sliders[id_Ref].get_Fill_Amount > value)
                    {
                        value = m_Score_Sliders[id_Ref].get_Fill_Amount;
                        index = i;
                    }
                }
            }

            return m_Player_Transforms[index];
        }
    }
    public Score_Slider[] get_Score_Sliders { get { return m_Score_Sliders; } }
    public Transform[] get_Players { get { return m_Player_Transforms; } }


    private void Awake ()
    {
        m_Transform = GetComponent<Transform>();
        m_Game_Controller = m_Transform.GetComponent<Game_Controller>();
        m_Fire_Cracker_Controller = m_Transform.GetComponent<Firecracker_Controller>();
    }

    
    private void Start()
    {
        GameObject[] points = GameObject.FindGameObjectsWithTag("Spawn_Point");
        m_Spawn_Transforms = new Transform[points.Length];
        GameObject[] sliders = GameObject.FindGameObjectsWithTag("Score_Slider");
        m_Score_Sliders = new Score_Slider[sliders.Length];
        m_Slider_Values = new float[sliders.Length];

        for (int i = 0; i < points.Length; i++)
        {
            int index = (int)points[i].GetComponent<Spawn_Point_ID>().spawn_Point;
            m_Spawn_Transforms[index] = points[i].GetComponent<Transform>();
        }

        for (int i = 0; i < sliders.Length; i++)
        {
            int index = (int)sliders[i].GetComponent<Score_Slider>().player_ID_Reference;
            m_Score_Sliders[index] = sliders[i].GetComponent<Score_Slider>();
            m_Score_Sliders[index].set_Slider_Overlay_Color = slider_Overlay_Color[index];
        }

        GameObject player_Data = GameObject.FindGameObjectWithTag("Load_Game_Data");
        m_Players_In_Game = player_Data.GetComponent<Game_Scene_Data>().activated_Players;
        Destroy(player_Data);

        StartCoroutine(Start_Placement_Itterator());
    }

    
    private void Update()
    {
        if (m_Player_Spawned_In && !m_Game_Over)
        {
            float top_Score = 0.0f;

            Add_Player_Score(ref top_Score);

            if (top_Score >= 1.0f)
            {
                m_Game_Controller.On_Win_Event();
                m_Fire_Cracker_Controller.On_Win_Event();
                Sort_Win_Player();
            }
        }
    }


    private void Sort_Win_Player()
    {
        float[] scores = new float[4];
        for (int i = 0; i < m_Player_Transforms.Length; i++)
        {
            //if (m_Player_Transforms[i] != null)
            //    m_Player_Transforms[i].GetComponent<Player_Movement_Controller>().enabled = false;
            scores[i] = m_Score_Sliders[i].get_Fill_Amount;
        }

        Array.Sort(scores, m_Score_Sliders);
        Array.Reverse(m_Score_Sliders);

        GameObject win_Data_Holder = new GameObject();
        win_Data_Holder.tag = "Load_Win_Data";
        Win_Scene_Data data = win_Data_Holder.AddComponent<Win_Scene_Data>();
        data.m_Player_Win_Rank = new Player_ID[m_Active_Player_Amounts];
        
        for (int i = 0; i < m_Active_Player_Amounts; i++)
        {
            data.m_Player_Win_Rank[i] = m_Score_Sliders[i].player_ID_Reference;
        }

        m_Game_Over = true;
    }


    private void Add_Player_Score (ref float top_Score)
    {
        for (int i = 0; i < m_Player_Transforms.Length; i++)
        {
            if (m_Player_Transforms[i] != null)
            {
                bool in_Water = m_Player_Transforms[i].GetComponent<Player_Movement_Controller>().In_Water;
                int id_Ref = (int)m_Player_Transforms[i].GetComponent<Player_Graphic_Controller>().get_Player_ID;

                if (in_Water)
                {
                    float delta_Value = target_Water_Time / 60.0f;
                    float prev_Value = m_Score_Sliders[id_Ref].get_Fill_Amount;
                    m_Score_Sliders[id_Ref].On_Waterfall_Point((delta_Value * Time.deltaTime), prev_Value);
                    //ADD THE SHIT
                    m_Score_Sliders[id_Ref].Foam_Active(true);
                    m_Score_Sliders[id_Ref].Sponge_Active(true);
                }
                else
                {
                    m_Score_Sliders[id_Ref].Foam_Active(false);
                    m_Score_Sliders[id_Ref].Sponge_Active(false);
                }
            }
            if (m_Score_Sliders[i].get_Fill_Amount > top_Score)
            {
                top_Score = m_Score_Sliders[i].get_Fill_Amount;
            }
        }
    }


    private void Instantiate_Players ()
    {
        m_Player_Transforms = new Transform[m_Players_In_Game.Length];

        for (int i = 0; i < m_Players_In_Game.Length; i++)
        {
            if (m_Players_In_Game[i])
            {
                m_Player_Transforms[i] = Instantiate(Resources.Load("Player/Player_Prefab", typeof(Transform))) as Transform;
                m_Player_Transforms[i].BroadcastMessage("Initialize_Player_ID", (Player_ID)i);
                m_Player_Transforms[i].BroadcastMessage("Initialize_Player", m_Game_Controller);
                m_Active_Player_Amounts++;
            }
            else
            {
                m_Player_Transforms[i] = null;
            }
        }

        Array.Sort(m_Slider_Values, m_Player_Transforms);
        Array.Reverse(m_Player_Transforms);

        for (int i = 0; i < m_Player_Transforms.Length; i++)
        {
            if (m_Player_Transforms[i] != null)
            {
                m_Player_Transforms[i].position = m_Spawn_Transforms[i].position;
                m_Player_Transforms[i].rotation = Math_Ex.get_Look_Rotation(m_Player_Transforms[i].position, m_Game_Controller.Track_Origin);
                float offset = m_Player_Transforms[i].GetComponent<Player_Movement_Controller>().Ground_Distance;
                m_Player_Transforms[i].position += m_Player_Transforms[i].up * offset;
            }
        }

        m_Player_Spawned_In = true;
        m_Game_Controller.On_Start_Event();
        m_Transform.GetComponent<Camera_Manager>().Initialize(m_Player_Transforms);
        m_Transform.GetComponent<Pickup_Manager>().Initialize(m_Active_Player_Amounts);
        m_Transform.FindChild("Start_Graphic").GetComponentInParent<Animator>().SetBool("Start", true);
    }


    private IEnumerator Start_Placement_Itterator()
    {
        bool is_Full = false;

        while (!is_Full)
        {
            float slider_Value = 0.0f;
            for (int i = 0; i < m_Score_Sliders.Length; i++)
            {
                if (m_Players_In_Game[i])
                {
                    float prev_Value = m_Score_Sliders[i].get_Fill_Amount;
                    float value = (get_Interact_Button((Player_ID)i, Button_Event_State.Down)) ? button_Hit_Fill_Amount : 0.0f;
                    m_Score_Sliders[i].On_Value_Change(prev_Value + value);
                    m_Slider_Values[i] = Mathf.Clamp(prev_Value + value, 0.0f, 1.0f);
                    if ((prev_Value + value) > slider_Value)
                        slider_Value = prev_Value + value;
                }
            }
            if (slider_Value >= 1.0f)
                is_Full = true;
            yield return null;
        }

        for (int i = 0; i < m_Score_Sliders.Length; i++)
        {
            m_Score_Sliders[i].On_Value_Change(0.0f);
        }

        Instantiate_Players();
        yield return null;
    }
    

    public void On_Object_Pickup(Transform player_Transform, float value)
    {
        for (int i = 0; i < m_Player_Transforms.Length; i++)
        {
            if (m_Player_Transforms[i] == player_Transform)
            {
                int ref_ID = (int)m_Player_Transforms[i].GetComponent<Player_Graphic_Controller>().get_Player_ID;
                float curr_Value = m_Score_Sliders[ref_ID].get_Fill_Amount;
                m_Score_Sliders[ref_ID].On_Coin_Pickup(value, curr_Value);
                m_Score_Sliders[ref_ID].On_Pickup_Present();
            }
        }
    }


    public void On_Game_Start()
    {
        for (int i = 0; i < m_Player_Transforms.Length; i++)
        {
            if (m_Player_Transforms[i] != null)
            {
                m_Player_Transforms[i].GetComponent<Rigidbody2D>().isKinematic = false;
                m_Player_Transforms[i].GetComponent<Player_Movement_Controller>().enabled = true;
            }
        }
    }


    public void On_Score_Change (Transform player, float change)
    {
        for (int i = 0; i < m_Player_Transforms.Length; i++)
        {
            if (m_Player_Transforms[i] != null && m_Player_Transforms[i] == player)
            {
                int ref_ID = (int)m_Player_Transforms[i].GetComponent<Player_Graphic_Controller>().get_Player_ID;
                float curr_Value = m_Score_Sliders[ref_ID].get_Fill_Amount;
                m_Score_Sliders[ref_ID].On_Value_Change(curr_Value - change);
            }
        }
    }


}



