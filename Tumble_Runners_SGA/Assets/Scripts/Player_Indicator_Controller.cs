﻿using UnityEngine;
using System.Collections;
using Player_Collection;
using Utility_Collection;


public class Player_Indicator_Controller : MonoBehaviour
{

    private Transform m_Transform;
    private Transform m_Arrow_Transform;
    private Transform m_Player_Transform;
    private SpriteRenderer[] m_Sprite_Renderers;
	

	private void Awake ()
    {
        m_Transform = GetComponent<Transform>();
        m_Arrow_Transform = m_Transform.FindChild("Arrow");
        m_Sprite_Renderers = new SpriteRenderer[2];
        m_Sprite_Renderers[0] = m_Transform.GetComponentInChildren<SpriteRenderer>();
        m_Sprite_Renderers[1] = m_Arrow_Transform.GetComponentInChildren<SpriteRenderer>();
    }
	
	
	private void Update ()
    {
        m_Arrow_Transform.rotation = Math_Ex.get_Look_Rotation(m_Transform.position, m_Player_Transform.position);
	}


    public void Initialize(Transform player_Transform)
    {
        m_Player_Transform = player_Transform;
        m_Sprite_Renderers[0].enabled = false;
        m_Sprite_Renderers[1].enabled = false;
    }


    public void Set_Active(bool value)
    {
        m_Sprite_Renderers[0].enabled = value;
        m_Sprite_Renderers[1].enabled = value;
    }


}



