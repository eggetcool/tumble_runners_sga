﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Timer_Script : MonoBehaviour
{
    private Pickup_Manager m_Timer_Script;
    private Text m_text;
    private CanvasRenderer im;
    private Image m_Coin_Object;
    private Image m_Firecracker_Object;
    private bool Blink;
    private bool change_Icon = true;
    private float m_Timer_Original;
    private bool m_Coin_Round;
    private float m_Timer;
    public Game_Controller m_Game_Controller;


    void Start()
    {
        m_Timer_Script = GameObject.Find("Scene").GetComponent<Pickup_Manager>();
        m_text = GetComponentInChildren<Text>();
        im = GetComponentInChildren<CanvasRenderer>();

        m_Coin_Object = GameObject.Find("Coin_Icon").GetComponent<Image>();
        m_Firecracker_Object = GameObject.Find("Firecracker_Icon").GetComponent<Image>();

        m_Firecracker_Object.enabled = false;
        m_Timer = 15.0f;

        m_Timer_Original = m_Timer_Script.spawn_In_Time;
        m_Coin_Round = true;
        GameObject _object = GameObject.Find("Scene");
        m_Game_Controller = _object.GetComponent<Game_Controller>();
    }


    void Update()
    {
        if (m_Game_Controller.Game_Over || !m_Game_Controller.Game_Started)
            return;

        m_Timer -= Time.deltaTime;
        m_text.text = Mathf.Round(m_Timer).ToString();

        if (m_Timer < 6.5f)
        {
            StartCoroutine(FadeOutNIn());
            Blink = true;

            if (m_Timer <= 0)
            {

                if (change_Icon)
                {
                    m_Coin_Object.enabled = !m_Coin_Object.enabled;
                    m_Firecracker_Object.enabled = !m_Firecracker_Object.enabled;
                    change_Icon = false;
                    m_Timer = 15.0f;
                }
            }
        }
        if (Mathf.Approximately(m_Timer, 15.0f))
        {
            change_Icon = true;
        }
        else
        {
            Blink = false;
        }
    }

    IEnumerator FadeOutNIn()
    {
        while (Blink)
        {
            m_text.fontSize = 25;
            yield return new WaitForSeconds(0.5f);
            m_text.fontSize = 30;
            yield return new WaitForSeconds(0.5f);
        }
    }
}