﻿using UnityEngine;
using System.Collections;

public class Delete_Item : MonoBehaviour {

    public float life_Time;


	private void Start ()
    {
        StartCoroutine(Destroy_Object());
	}


    private IEnumerator Destroy_Object ()
    {
        yield return new WaitForSeconds(life_Time);
        Destroy(this.gameObject);
    }


}



