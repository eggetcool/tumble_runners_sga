﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using Player_Collection;
using Utility_Collection;


public class Game_Controller : MonoBehaviour
{

    public Transform track;
    public float track_Radius;
    public float track_Rotation_Speed;
    public float track_Rotation_Time;
    public float track_Direction_Change_Speed;
    public float waterfall_Rotation;

    public Transform Track_Transform { get { return track; } }
    public Vector2 Track_Origin { get { return track.position; } }
    public float Track_Torque {
        get {
            float circumference = 2.0f * Mathf.PI * track_Radius;
            float delta_Time = 360.0f / m_Rotation_Speed * (int)m_Rotation_Direction;
            return circumference / delta_Time;
        }
    }
    private bool m_Game_Over;
    private bool m_Game_Started;

    public bool Game_Over { get { return m_Game_Over; } }
    public bool Game_Started { get { return m_Game_Started; } }

    private enum Rotation_Change
    {
        Start,
        Stop
    }

    private Transform m_Transform;
    private Transform m_Waterfall_Transform;
    private Animator m_Waterfall_Animator;
    private SpriteRenderer m_Waterfall_Sprite_Renderer;
    private PolygonCollider2D[] m_Polygon_Colliders;
    private Obstacle_Manager m_Obstacle_Manager;
    private Pickup_Manager m_Pickup_Manager;
    private Rotation_Direction m_Rotation_Direction;
    private float m_Rotation_Speed;
    private float m_Rotation_Timer;
    private bool m_Rotation_Change;
    
    private void Awake ()
    {
        m_Transform = GetComponent<Transform>();
        m_Waterfall_Transform = m_Transform.FindChild("Waterfall");
        m_Waterfall_Animator = m_Waterfall_Transform.GetComponent<Animator>();
        m_Waterfall_Sprite_Renderer = m_Waterfall_Transform.GetComponent<SpriteRenderer>();
        m_Polygon_Colliders = m_Waterfall_Transform.GetComponents<PolygonCollider2D>();
        m_Obstacle_Manager = m_Transform.GetComponent<Obstacle_Manager>();
        m_Pickup_Manager = m_Transform.GetComponent<Pickup_Manager>();
    }


    private void Start()
    {
        m_Rotation_Direction = Rotation_Direction.Left;
        m_Rotation_Speed = 0.0f;
        m_Rotation_Timer = track_Rotation_Time;
        m_Rotation_Change = true;
    }

    
    private void Update()
    {
        track.Rotate(Vector3.forward, m_Rotation_Speed * Time.deltaTime);

        if (m_Game_Over)
            return;

        if (!m_Rotation_Change)
        {
            m_Rotation_Timer -= Time.deltaTime;

            if (m_Rotation_Timer <= 0.0f)
            {
                m_Rotation_Timer = track_Rotation_Time;
                m_Rotation_Change = true;

                if (m_Rotation_Direction == Rotation_Direction.Left) {
                    StartCoroutine(Change_Rotation_Direction(Rotation_Direction.Right));
                }
                else {
                    StartCoroutine(Change_Rotation_Direction(Rotation_Direction.Left));
                }
            }
        }
    }


    public void On_Win_Event ()
    {
        if (m_Rotation_Change)
        {
            StopAllCoroutines();
        }
        StartCoroutine(Initialize_Rotation_Direction(Rotation_Change.Stop));
    }


    public void On_Start_Event ()
    {
        StartCoroutine(Initialize_Rotation_Direction(Rotation_Change.Start));
    }


    private IEnumerator Change_Rotation_Direction(Rotation_Direction target_Rotation_Direction)
    {
        float target_Value = 0.0f;
        m_Rotation_Direction = target_Rotation_Direction;
        m_Waterfall_Animator.SetBool("End", true);

        while (m_Rotation_Speed != target_Value)
        {
            m_Rotation_Speed = Math_Ex.Const_Lerp(m_Rotation_Speed, target_Value, track_Direction_Change_Speed, Time.deltaTime);
            if (Mathf.Approximately(m_Rotation_Speed, target_Value))
            {
                m_Rotation_Speed = target_Value;
                break;
            }
            yield return null;
        }

        target_Value = track_Rotation_Speed * (int)target_Rotation_Direction;
        m_Waterfall_Animator.SetBool("Start", true);
        On_Waterfall_Change(target_Rotation_Direction);
        m_Obstacle_Manager.On_Direction_Change();
        m_Pickup_Manager.On_Direction_Change(target_Rotation_Direction);

        while (m_Rotation_Speed != target_Value)
        {
            m_Rotation_Speed = Math_Ex.Const_Lerp(m_Rotation_Speed, target_Value, track_Direction_Change_Speed, Time.deltaTime);
            if (Mathf.Approximately(m_Rotation_Speed, target_Value))
            {
                m_Rotation_Speed = target_Value;
                break;
            }
            yield return null;
        }

        m_Rotation_Change = false;
    }


    private void On_Waterfall_Change (Rotation_Direction target_Rotation_Direction)
    {
        Vector3 new_Pos = m_Waterfall_Transform.position;
        new_Pos.x *= -1.0f;
        m_Waterfall_Transform.position = new_Pos;
        if (target_Rotation_Direction == Rotation_Direction.Left)
        {
            m_Waterfall_Transform.rotation = Quaternion.Euler(0.0f, 0.0f, -waterfall_Rotation);
            m_Polygon_Colliders[1].enabled = false;
            m_Polygon_Colliders[0].enabled = true;
            m_Waterfall_Sprite_Renderer.flipX = false;
        }
        else
        {
            m_Waterfall_Transform.rotation = Quaternion.Euler(0.0f, 0.0f, waterfall_Rotation);
            m_Polygon_Colliders[0].enabled = false;
            m_Polygon_Colliders[1].enabled = true;
            m_Waterfall_Sprite_Renderer.flipX = true;
        }
    }


    private IEnumerator Initialize_Rotation_Direction(Rotation_Change rotation_Change)
    {
        float target_Value = (rotation_Change == Rotation_Change.Stop) ? 0.0f : track_Rotation_Speed * (int)m_Rotation_Direction;
        if (rotation_Change == Rotation_Change.Start)
        {
            m_Waterfall_Animator.SetBool("Start", true);
        }
        else
        {
            m_Waterfall_Animator.SetBool("End", true);
        }

        while (m_Rotation_Speed != target_Value)
        {
            m_Rotation_Speed = Math_Ex.Const_Lerp(m_Rotation_Speed, target_Value, track_Direction_Change_Speed, Time.deltaTime);
            if (Mathf.Approximately(m_Rotation_Speed, target_Value))
            {
                m_Rotation_Speed = target_Value;
                break;
            }
            yield return null;
        }

        m_Rotation_Change = false;

        if (rotation_Change == Rotation_Change.Stop)
        {
            SceneManager.LoadScene("Victory_scene");
        }
    }


    public void Start_Game ()
    {
        m_Transform.SendMessage("On_Game_Start", SendMessageOptions.DontRequireReceiver);
        m_Game_Started = true;
    }
}
