﻿using UnityEngine;
using System.Collections;

namespace Player_Collection
{

    public enum Player_ID
    {
        One,
        Two,
        Three,
        Four
    }

    public enum Player_Direction
    {
        Left = -1,
        Right = 1
    }
}
